<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UsuarioModel extends CI_Model{

    public function criar(){
        if(! sizeof($_POST)) return;
        $nome = $this->input->post('nome');
        $sobrenome = $this->input->post('sobrenome');
        $endereco = $this->input->post('endereco');
        $complemento = $this->input->post('complemento');
        $bairro = $this->input->post('bairro');
        $oleo = $this->input->post('oleo');
        $email = $this->input->post('email');
        $celular = $this->input->post('celular');
        $senha = $this->input->post('senha');

        $sql = "INSERT INTO usuario(nome, sobrenome, endereco, complemento, bairro, oleo, email, celular, senha) 
        VALUES ('$nome', '$sobrenome', '$endereco', '$complemento', '$bairro', '$oleo', '$email', '$celular', '$senha')";
        $this->db->query($sql);

    }

    public function listar()
    {
        $sql = "SELECT * FROM usuario";
        $rs = $this->db->query($sql);
        $m = $rs->result();
        $html = '';
        
        foreach ($m as $row) {
            $html .= "<tr>";
            $html .= "<td>$row->nome</td>";
            $html .= "<td>$row->sobrenome</td>";
            $html .= "<td>$row->endereco</td>";
            $html .= "<td>$row->complemento</td>";
            $html .= "<td>$row->bairro</td>";
            $html .= "<td>$row->oleo</td>";
            $html .= "<td>$row->email</td>";
            $html .= "<td>$row->celular</td>";
            $html .= '<td>'.$this->get_action_buttons($row->id).'</td>';
            $html .= "</tr>";

        }
        return $html;
    }

    private function get_action_buttons($id){
        $html = '<a href="'.base_url('usuario/atualizar/'.$id).'">
        <i class="fas fa-edit mr-3 text-primary"></i></a>';
        $html .= '<a href="'.base_url('usuario/remover/'.$id).'">
        <i class="fas fa-trash-alt mr-3 text-danger"></i></a>';
        $html .= '<a href="'.base_url('usuario/detalhe/'.$id).'">
        <i class="fas fa-search mr-3 text-muted"></i></a>';
        return $html;
    }

    public function Detalhe($id){
        //PEGA OS DADOS DO BANCO DE DADOS
        $sql = "SELECT * FROM usuario WHERE id = $id";
        $rs = $this->db->query($sql);
        return $rs->result_array()[0];
    }

    public function atualizar($id){
        if(! sizeof($_POST)) return;
 
        $data = $this->input->post();
        //ACTIVERECORD pesquisar em casa.
        $this->db->update('usuario', $data, "id = $id");
        redirect('usuario');
    }

    public function remover($id){
        $this->db->delete('usuario', "id = $id");
    }


}

