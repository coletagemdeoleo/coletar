<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class usuario extends CI_Controller{

    /*
    *LISTA DE USUARIOS   
    */
    public function index(){
        $this->load->view('common/header');
        $this->load->view('common/navbar');
        $this->load->model('UsuarioModel');
        $data ['table'] = $this->UsuarioModel->listar();
        $this->load->view('usuario/tabela_usuario', $data);
        $this->load->view('common/footer');    
    }

    /*
    *CADASTRAMENTO DE USUARIOS   
    */
    public function criar(){
        $this->load->view('common/header');
        $this->load->view('common/navbar');
        $this->load->model('UsuarioModel');
        $this->UsuarioModel->criar();
        $a['titulo'] = "Cadastro de cliente";
        $a['btn'] = "Cadastrar";
        $this->load->view('usuario/cadastro_usuario', $a);
        $this->load->view('common/footer');    
    }

    /*
    * DETALHES DOS USUARIOS 
    */
    public function detalhe($id){
        $this->load->view('common/header');
        $this->load->view('common/navbar');
        $this->load->model('UsuarioModel');
        $a['usuario'] = $this->UsuarioModel->Detalhe($id);
        $this->load->view('usuario/detalhe_usuario', $a);
		$this->load->view('common/footer');
    }

    /*
    * EDITAR OS USUARIOS 
    */
    public function atualizar($id){
        $this->load->view('common/header');
        $this->load->view('common/navbar');
        $this->load->model('UsuarioModel');
        $this->UsuarioModel->atualizar($id);
        $a['usuario'] = $this->UsuarioModel->detalhe($id);
        $a['titulo'] = "Atualização do cliente";
        $a['btn'] = "Atualizar";
        $this->load->view('usuario/cadastro_usuario', $a);
		$this->load->view('common/footer');
    }

    /*
    * REMOVER OS USUARIOS 
    */
    public function remover($id){
        $this->load->model('UsuarioModel');
        $this->UsuarioModel->remover($id);
        redirect('usuario');
    }

    
}