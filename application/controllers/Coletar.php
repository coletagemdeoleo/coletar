<?php 
class coletar extends CI_Controller {

	public function index()
	{
		$this->load->view('common/header');
        $this->load->view('common/navbar');

		$this->load->view('common/footer');
    }    
    
	public function empresa($id = 1)
	{
		$this->load->view('common/header');
        $this->load->view('common/navbar');
        $this->load->model('EmpresaModel');
        $a = $this->EmpresaModel->compromisso($id);
        $data ['img']= $this->load->view('empresa/imagem', null, true);
        $data ['text']= $this->load->view('empresa/inicio', $a, true);
        $data ['meio'] = $this->load->view('empresa/meio', $a, true);
        $data ['valores'] = $this->load->view('empresa/valores', $a, true);
        $this->load->view('empresa/layout', $data);
		$this->load->view('common/footer');
    }    
    
	public function beneficiamento()
	{
		$this->load->view('common/header');
        $this->load->view('common/navbar');

		$this->load->view('common/footer');
    }    
    
	public function servicos()
	{
		$this->load->view('common/header');
        $this->load->view('common/navbar');

		$this->load->view('common/footer');
	}    
}
