<nav class="mb-1 navbar navbar-expand-lg navbar-dark black" style="background: linear-gradient(to right, #ffffff 10%, #8bc34a 100%);">
  <a class="navbar-brand" style="color: black" href="<?= base_url(); ?>">Coletar</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-333"
    aria-controls="navbarSupportedContent-333" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarSupportedContent-333">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" style="color: black" href="<?= base_url(); ?>">Home
          <span class="sr-only">(current)</span>
        </a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" style="color: black" href="<?= base_url('Coletar/empresa'); ?>">Empresa
        </a>
      </li>      
      <li class="nav-item active">
        <a class="nav-link" style="color: black" href="<?= base_url('Coletar/beneficiamento'); ?>">Beneficiamento
        </a>
      </li>      
      <li class="nav-item active">
        <a class="nav-link" style="color: black" href="<?= base_url('Coletar/beneficiamento'); ?>">Serviços
        </a>
      </li>      
      <li class="nav-item active">
        <a class="nav-link" style="color: black" href="<?= base_url('Usuario'); ?>">Lista de cadastrados
        </a>
      </li>      
      <li class="nav-item ">
        <a class="nav-link" style="color: black" href="<?= base_url('Usuario/criar'); ?>">Cadastre-se
        </a>
      </li>
    </ul>
    <ul class="navbar-nav ml-auto nav-flex-icons">
      <li class="nav-item">
        <a class="nav-link waves-effect waves-light">
          <i class="fab fa-facebook"></i>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link waves-effect waves-light">
          <i class="fab fa-instagram"></i>
        </a>
      </li>    
    </ul>
  </div>
</nav>
<!--/.Navbar -->