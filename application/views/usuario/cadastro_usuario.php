<div class="container">
    <div class="row">
        <div class="col-md-12 mx-auto mt-5">
            <form method="POST" class="text-center border-light p-5">
                <p class="h4 mb-4"><?= $titulo ?></p>
                <div class="form-row mb-4">
                    <div class="col-md-6">
                        <input type="text" value="<?= isset($usuario['nome']) ? $usuario['nome'] : '' ?>" id="nome" name="nome" class="form-control" placeholder="Nome">
                    </div>
                    <div class="col-md-6">
                        <input type="text" value="<?= isset($usuario['sobrenome']) ? $usuario['sobrenome'] : '' ?>" id="sobrenome" name="sobrenome" class="form-control" placeholder="Sobrenome">
                    </div>
                </div>
                <div class="form-row mb-4">
                    <div class="col-md-7">
                        <input type="text" value="<?= isset($usuario['endereco']) ? $usuario['endereco'] : '' ?>" id="endereco" name="endereco" class="form-control" placeholder="Endereco">
                    </div>
                    <div class="col-md-5">
                        <input type="text" value="<?= isset($usuario['complemento']) ? $usuario['complemento'] : '' ?>" id="complemento" name="complemento" class="form-control" placeholder="Complemento">
                    </div>
                </div>
                <div class="form-row mb-4">
                    <div class="col-md-8">
                        <input type="text" value="<?= isset($usuario['bairro']) ? $usuario['bairro'] : '' ?>" id="bairro" name="bairro" class="form-control" placeholder="Bairro">
                    </div>
                    <div class="col-md-4">
                        <input type="text" value="<?= isset($usuario['oleo']) ? $usuario['oleo'] : '' ?>" id="oleo" name="oleo" class="form-control" placeholder="Quantidade de óleo">
                    </div>
                </div>
                <div class="form-row mb-4">
                    <div class="col-md-7">
                        <input type="text" value="<?= isset($usuario['email']) ? $usuario['email'] : '' ?>" id="email" name="email" class="form-control" placeholder="E-mail">
                    </div>
                    <div class="col-md-5">
                        <input type="text" value="<?= isset($usuario['celular']) ? $usuario['celular'] : '' ?>" id="celular" name="celular" class="form-control" placeholder="Celular">
                    </div>
                </div>
                <input type="password" value="<?= isset($usuario['senha']) ? $usuario['senha'] : '' ?>" id="senha" name="senha" class="form-control" placeholder="Senha" aria-describedby="defaultRegisterFormPasswordHelpBlock"><br>
                <button class="btn btn-light-green my-4 btn-block" type="submit"><?= $btn ?></button>
            </form>
        </div>
    </div>
</div>