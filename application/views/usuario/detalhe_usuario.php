<div class="container">
    <div class="row">
        <div class="col-md-8 mx-auto">
            <div class="card">
                <img class="card-img-top" src="https://mdbootstrap.com/img/Photos/Others/images/3.jpg" alt="Card image cap">
                <div class="card-body">
                    <h4 class="card-title"><a><?= $usuario['nome'].' '.$usuario['sobrenome']  ?></a></h4>
                    <p class="card-title"><a><b>Email:</b> <?= $usuario['email']  ?></a></p>
                    <p class="card-title"><a><b>Endereço:</b> <?= $usuario['endereco'].', '.$usuario['complemento'].', '.$usuario['bairro']  ?></a></p>
                    <p class="card-title"><a><b>Quantidade de oleo:</b> <?= $usuario['oleo']  ?></a></p>
                    <p class="card-title"><a><b>Celular:</b> <?= $usuario['celular']  ?></a></p>
                    <p class="card-title"><a><b>Dia de cadastrado:</b> <?= $usuario['last_modified']  ?></a></p>
                </div>
            </div>
        </div>
    </div>
</div>