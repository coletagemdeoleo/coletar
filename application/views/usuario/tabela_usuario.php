<div class="container">
    <div class="row">
        <div class="col-md-12 mx-auto mt-5">
            <table class="table">
                <thead class="light-green white-text">
                    <tr>
                    <th scope="col">Nome</th>
                    <th scope="col">Sobrenome</th>
                    <th scope="col">Endereço</th>
                    <th scope="col">Complemento</th>
                    <th scope="col">Bairro</th>
                    <th scope="col">Oleo</th>
                    <th scope="col">E-mail</th>
                    <th scope="col">Celular</th>
                    <th scope="col"></th>
                    </tr>
                </thead>
                <tbody>   
                    <?= $table ?>
                </tbody>        
        </div>
    </div>
</div>