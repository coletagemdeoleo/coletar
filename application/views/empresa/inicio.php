<div class="container">
  <div class="row">
    <div class=" mx-auto mt-5">
      <h4 class="h4 mb-4" style= "text-align:center"><?= $titulo ?> </h4>
      <p class="font-weight-normal">
          <?= $subtitulo ?>
      </p>
      <p class="font-weight-normal">
        <?= $complemento ?>
      </p>
    </div>
  </div>
</div>