<div class="container" style= "text-align:center" >
    <div class="row mt-5">
        <div class="col-sm">
            <h4 class="h4 mb-4">Missão</h4>
            <p style="text-center">
                <?= $missao ?>
            </p>
        </div>
        <div class="col-sm">
            <h4 class="h4 mb-4">Visão</h4>
            <p>
                <?= $visao ?>
            </p>
        </div>
        <div class="col-sm">
            <h4 class="h4 mb-4">Valores</h4>
            <p>
                <?= $valores ?>
            </p>
        </div>
    </div>
</div>